use actix_web::{web, App, HttpResponse, HttpServer, Responder};
use jagdlib::funcs::*;
use jagdlib::types;
use std::sync::Mutex;

pub const DATA: &str = "/usr/share/jagenlernen/data/bindata";

struct AppState {
    db: Mutex<types::Db>,
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    let state = web::Data::new(AppState {
        db: Mutex::new(load("/usr/share/jagenlernen/data/bindata")),
    });
    //let safe: Option<types::Db> = Some(load("/usr/share/jagenlernen/data/data"));
    HttpServer::new(move || {
        App::new()
            .app_data(state.clone())
            .route("/get/get_audio", web::get().to(get_audio))
            .route("/get/get_metadb", web::get().to(get_metadb))
            .route("/get/get_metaquests", web::get().to(get_metaquests))
            .route("/get/next", web::get().to(next))
            .route("/get/edit_history", web::get().to(edit_history))
            .route("/get/learn_history", web::get().to(learn_history))
            .route("/mod/save", web::get().to(save))
            .route("/mod/queue", web::get().to(queue))
            .route("/mod/set_mode", web::post().to(set_mode))
    })
    .bind(("127.0.0.1", 8080))?
    .run()
    .await
}
async fn get_audio() -> impl Responder {
    let audio = jagdlib::types::load_audio();
    let bin = bincode::serialize(&audio).unwrap();
    HttpResponse::Ok().body(bin)
}

async fn save(data: web::Data<AppState>) -> impl Responder {
    let mut db = data.db.lock().unwrap();
    db.save();
    let x = db.save_to_disk(DATA.to_string());
    HttpResponse::Ok().body(format!("{:?}", x))
}
async fn queue(data: web::Data<AppState>, bytes: web::Bytes) -> impl Responder {
    let question: types::Question = bincode::deserialize(&bytes[..]).unwrap();
    if let Ok(db) = &mut data.db.lock() {
        db.queue(question);
    }
    HttpResponse::Ok().body("ok")
}
async fn set_mode(data: web::Data<AppState>, _bytes: web::Bytes) -> impl Responder {
    let mode: types::Mode = bincode::deserialize(&_bytes[..]).unwrap();
    if let Ok(db) = &mut data.db.lock() {
        db.set_mode(mode);
    }
    HttpResponse::Ok().body("ok")
}
async fn next(data: web::Data<AppState>) -> impl Responder {
    let mut quest: Result<types::Question, String> = Err("Couldn't unlock mutex".to_string());
    if let Ok(db) = &mut data.db.lock() {
        quest = db.next_one()
    }
    let bin: Vec<u8> = bincode::serialize(&quest).unwrap();
    HttpResponse::Ok().body(bin)
}
async fn get_metadb(data: web::Data<AppState>) -> impl Responder {
    let mut metadb: Result<types::MetaDb, String> = Err("Couldn't unlock mutex".to_string());
    if let Ok(db) = &mut data.db.lock() {
        metadb = db.get_metadb()
    }
    let bin: Vec<u8> = bincode::serialize(&metadb).unwrap();
    HttpResponse::Ok().body(bin)
}

async fn learn_history(data: web::Data<AppState>) -> impl Responder {
    let mut history: Result<types::LearnHistory, String> = Err("Couldn't unlock mutex".to_string());
    if let Ok(db) = &mut data.db.lock() {
        history = db.learn_history()
    }
    let bin: Vec<u8> = bincode::serialize(&history).unwrap();
    HttpResponse::Ok().body(bin)
}
async fn edit_history(data: web::Data<AppState>) -> impl Responder {
    let mut history: Result<types::EditHistory, String> = Err("Couldn't unlock mutex".to_string());
    if let Ok(db) = &mut data.db.lock() {
        history = db.edit_history()
    }
    let bin: Vec<u8> = bincode::serialize(&history).unwrap();
    HttpResponse::Ok().body(bin)
}
async fn get_metaquests(data: web::Data<AppState>, _bytes: web::Bytes) -> impl Responder {
    let mode: types::Mode = bincode::deserialize(&_bytes[..]).unwrap();
    let mut themas: Result<types::MetaQuests, String> = Err("Couldn't unlock mutex".to_string());
    if let Ok(db) = &mut data.db.lock() {
        themas = db.get_metaquests(mode)
    }
    let bin: Vec<u8> = bincode::serialize(&themas).unwrap();
    HttpResponse::Ok().body(bin)
}
